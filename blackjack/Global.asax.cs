﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;

namespace blackjack
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        /// <summary>
        /// We find a way to use Session for our game
        /// http://stackoverflow.com/questions/11478244/asp-net-web-api-session-or-something
        /// </summary>
        public override void Init()
        {
            //
            //if (HttpContext.Current.Request.Url.AbsolutePath.StartsWith("/api/")) {
                this.PostAuthenticateRequest += MvcApplication_PostAuthenticateRequest;
            //}
            
            base.Init();
        }

        /// <summary>
        /// http://stackoverflow.com/questions/11478244/asp-net-web-api-session-or-something
        /// </summary>
        void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(
                SessionStateBehavior.Required);
        }
    }
}
