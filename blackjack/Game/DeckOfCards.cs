﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blackjack.Game
{
    /// <summary>
    /// Class representing structure for single Card
    /// TODO make it Entity
    /// </summary>
    public class Card
    {
        public int nr;
        public string sign;
        public string suit;
        public int[] points;

        public Card() { }
    }

    /// <summary>
    /// Structure deck of Cards
    /// TODO make it Entity
    /// </summary>
    public class DeckOfCards
    {
        public List<Card> Cards = new List<Card>();

        public DeckOfCards()
        {

            Cards.Add(new Card { nr = 1, sign = "A", suit = "Club", points = new int[] { 1, 11 }});
            Cards.Add(new Card { nr = 2, sign = "2", suit = "Club", points = new int[] { 2 } });
            Cards.Add(new Card { nr = 3, sign = "3", suit = "Club", points = new int[] { 3 } });
            Cards.Add(new Card { nr = 4, sign = "4", suit = "Club", points = new int[] { 4 } });
            Cards.Add(new Card { nr = 5, sign = "5", suit = "Club", points = new int[] { 5 } });
            Cards.Add(new Card { nr = 6, sign = "6", suit = "Club", points = new int[] { 6 } });
            Cards.Add(new Card { nr = 7, sign = "7", suit = "Club", points = new int[] { 7 } });
            Cards.Add(new Card { nr = 8, sign = "8", suit = "Club", points = new int[] { 8 } });
            Cards.Add(new Card { nr = 9, sign = "9", suit = "Club", points = new int[] { 9 } });
            Cards.Add(new Card { nr = 10, sign = "10", suit = "Club", points = new int[] { 10 }});
            Cards.Add(new Card { nr = 11, sign = "J", suit = "Club", points = new int[] { 10 }});
            Cards.Add(new Card { nr = 12, sign = "Q", suit = "Club", points = new int[] { 10 }});
            Cards.Add(new Card { nr = 13, sign = "K", suit = "Club", points = new int[] { 10 }});

            Cards.Add(new Card { nr = 14, sign = "A", suit = "Spade", points = new int[] { 1, 11 } });
            Cards.Add(new Card { nr = 15, sign = "2", suit = "Spade", points = new int[] { 2 } });
            Cards.Add(new Card { nr = 16, sign = "3", suit = "Spade", points = new int[] { 3 } });
            Cards.Add(new Card { nr = 17, sign = "4", suit = "Spade", points = new int[] { 4 } });
            Cards.Add(new Card { nr = 18, sign = "5", suit = "Spade", points = new int[] { 5 } });
            Cards.Add(new Card { nr = 19, sign = "6", suit = "Spade", points = new int[] { 6 } });
            Cards.Add(new Card { nr = 20, sign = "7", suit = "Spade", points = new int[] { 7 } });
            Cards.Add(new Card { nr = 21, sign = "8", suit = "Spade", points = new int[] { 8 } });
            Cards.Add(new Card { nr = 22, sign = "9", suit = "Spade", points = new int[] { 9 } });
            Cards.Add(new Card { nr = 23, sign = "10", suit = "Spade", points = new int[] { 10 } });
            Cards.Add(new Card { nr = 24, sign = "J", suit = "Spade", points = new int[] { 10 } });
            Cards.Add(new Card { nr = 25, sign = "Q", suit = "Spade", points = new int[] { 10 } });
            Cards.Add(new Card { nr = 26, sign = "K", suit = "Spade", points = new int[] { 10 } });

            Cards.Add(new Card { nr = 27, sign = "A", suit = "Heart", points = new int[] { 1, 11 } });
            Cards.Add(new Card { nr = 28, sign = "2", suit = "Heart", points = new int[] { 2 } });
            Cards.Add(new Card { nr = 29, sign = "3", suit = "Heart", points = new int[] { 3 } });
            Cards.Add(new Card { nr = 30, sign = "4", suit = "Heart", points = new int[] { 4 } });
            Cards.Add(new Card { nr = 31, sign = "5", suit = "Heart", points = new int[] { 5 } });
            Cards.Add(new Card { nr = 32, sign = "6", suit = "Heart", points = new int[] { 6 } });
            Cards.Add(new Card { nr = 33, sign = "7", suit = "Heart", points = new int[] { 7 } });
            Cards.Add(new Card { nr = 34, sign = "8", suit = "Heart", points = new int[] { 8 } });
            Cards.Add(new Card { nr = 35, sign = "9", suit = "Heart", points = new int[] { 9 } });
            Cards.Add(new Card { nr = 36, sign = "10", suit = "Heart", points = new int[] { 10 } });
            Cards.Add(new Card { nr = 37, sign = "J", suit = "Heart", points = new int[] { 10 } });
            Cards.Add(new Card { nr = 38, sign = "Q", suit = "Heart", points = new int[] { 10 } });
            Cards.Add(new Card { nr = 39, sign = "K", suit = "Heart", points = new int[] { 10 } });

            Cards.Add(new Card { nr = 40, sign = "A", suit = "Diamond", points = new int[] { 1, 11 } });
            Cards.Add(new Card { nr = 41, sign = "2", suit = "Diamond", points = new int[] { 2 } });
            Cards.Add(new Card { nr = 42, sign = "3", suit = "Diamond", points = new int[] { 3 } });
            Cards.Add(new Card { nr = 43, sign = "4", suit = "Diamond", points = new int[] { 4 } });
            Cards.Add(new Card { nr = 44, sign = "5", suit = "Diamond", points = new int[] { 5 } });
            Cards.Add(new Card { nr = 45, sign = "6", suit = "Diamond", points = new int[] { 6 } });
            Cards.Add(new Card { nr = 46, sign = "7", suit = "Diamond", points = new int[] { 7 } });
            Cards.Add(new Card { nr = 47, sign = "8", suit = "Diamond", points = new int[] { 8 } });
            Cards.Add(new Card { nr = 48, sign = "9", suit = "Diamond", points = new int[] { 9 } });
            Cards.Add(new Card { nr = 49, sign = "10", suit = "Diamond", points = new int[] { 10 } });
            Cards.Add(new Card { nr = 50, sign = "J", suit = "Diamond", points = new int[] { 10 } });
            Cards.Add(new Card { nr = 51, sign = "Q", suit = "Diamond", points = new int[] { 10 } });
            Cards.Add(new Card { nr = 52, sign = "K", suit = "Diamond", points = new int[] { 10 } });

        }
    }
}