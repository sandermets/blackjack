﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blackjack.Game
{
    public class BlackJack
    {
        private int _money;
        private List<Card> _cards;
        private List<int> _scores;
        private List<Card> _selected;
        private List<int> _currentPoints;
        private string _gameMessage;
        private bool _btnStart;
        private bool _btnDeal;
        private bool _btnBreak;
        private int _acesCount;
        private bool _breakNotActive;

        public int Money
        {
            get { return this._money; }
            set { this._money = value; }
        }

        public List<Card> Cards
        {
            get { return this._cards; }
            set { this._cards = value; }
        }

        public List<int> Scores
        {
            get { return this._scores; }
            set { this._scores = value; }
        }

        public List<Card> Selected
        {
            get { return this._selected; }
            set { this._selected = value; }
        }

        public List<int> CurrentPoints
        {
            get { return this._currentPoints; }
            set { this._currentPoints = value; }
        }

        public string GameMessage
        {
            get { return this._gameMessage; }
            set { this._gameMessage = value; }
        }

        public bool BtnStart
        {
            get { return this._btnStart; }
            set { this._btnStart = value; }
        }

        public bool BtnDeal
        {
            get { return this._btnDeal; }
            set { this._btnDeal = value; }
        }

        public bool BtnBreak
        {
            get { return this._btnBreak; }
            set { this._btnBreak = value; }
        }

        public int AcesCount
        {
            get { return this._acesCount; }
            set { this._acesCount = value; }
        }

        public bool BreakNotActive
        {
            get { return this._breakNotActive; }
            set { this._breakNotActive = value; }
        }

        public BlackJack()
        {
            this.Money = 100;
            DeckOfCards doc = new DeckOfCards();
            this.Cards = doc.Cards;
            this.BtnStart = false;
            this.BtnDeal = this.BtnBreak = false;
        }

        private void _refreshScore()
        {

            int ix = 1;
            this.AcesCount = 0;
            bool playerWon = false;

            this.CurrentPoints[0] = 0;

            while (ix < 6)
            {
                //reset ACE related combinations
                if (this.CurrentPoints[ix] != null)
                {
                    this.Cards.RemoveAt(ix);
                }
                ix++;
            }

            for (int i = 0; i < this.Selected.Count; i++)
            {
                this.AcesCount += this.Selected[i].sign == "A" ? 1 : 0;
                this.CurrentPoints[i] += this.Selected[i].points[0];
            }

            if (this.CurrentPoints[0] > 21)
            {
                this.GameMessage = "Better luck next time!";
                this.BtnStart = false;
                this.BtnDeal = this.BtnBreak = true;
            }

            if (this.AcesCount == 1)
            {
                int woA = this.CurrentPoints[0] - 1;
                this.CurrentPoints[1] = woA + 11;
            }
            else if (this.AcesCount == 2)
            {
                int woA = this.CurrentPoints[0] - 2;
                int r1 = woA + 1 + 1;
                if (r1 < 22)
                {
                    this.CurrentPoints[2] = r1;
                    int r2 = woA + 1 + 11;
                    if (r2 < 22)
                    {
                        this.CurrentPoints[2] = r2;
                    }
                }

            }
            else if (this.AcesCount == 3)
            {
                int woA = this.CurrentPoints[0] - 3;
                int r1 = woA + 1 + 1 + 1;
                if (r1 < 22)
                {
                    this.CurrentPoints[1] = r1;
                    int r2 = woA + 1 + 1 + 11;
                    if (r2 < 22)
                    {
                        this.CurrentPoints[2] = r2;
                    }
                }
            }
            else if (this.AcesCount == 4)
            {
                int woA = this.CurrentPoints[0] - 4;
                int r1 = woA + 1 + 1 + 1 + 1;
                if (r1 < 22)
                {
                    this.CurrentPoints[1] = r1;
                    int r2 = woA + 1 + 1 + 1 + 11;
                    if (r2 < 22)
                    {
                        this.CurrentPoints[2] = r2;
                    }
                }
            }

            for (int i = 0; i < this.CurrentPoints.Count; i++)
            {
                if (this.CurrentPoints[i] == 21)
                {
                    this.GameMessage = "You Win!!!";
                    this.BtnDeal = this.BtnBreak = true;
                    this.BtnStart = false;
                    if (this.BreakNotActive)
                    {
                        this.Money += 30;
                    }

                }
                else if (this.CurrentPoints[i] > 21 && i > 0)
                {
                    //do not hold scores over 21
                    this.CurrentPoints.RemoveAt(i);
                }
            }

        }

        public void RandomSelect()
        {
            this.BtnDeal = true;

            Random rnd = new Random();
            int i = rnd.Next(1, 52);


            if (this.Cards[i - 1] != null)
            {
                this.Selected.Add(this.Cards[i - 1]);
                this.Cards.RemoveAt(i - 1);

                if (this.Cards.Count > 0)
                {
                    this.BtnDeal = this.BtnBreak = false;
                }
            }
            else
            {
                this.RandomSelect();
            }

            this._refreshScore();
        }

    }
}