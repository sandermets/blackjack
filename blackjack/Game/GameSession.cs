﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blackjack.Game
{
    public class GameSession
    {
        private BlackJack _blackJack;

        public BlackJack BlackJack
        {
            get { return this._blackJack; }
            set { this._blackJack = value; }
        }

        public GameSession()
        {
            if (HttpContext.Current.Session["TheGame"] == null)
            {
                HttpContext.Current.Session["TheGame"] = new BlackJack();
            }
            this.BlackJack = (BlackJack)HttpContext.Current.Session["TheGame"]; 
        }

        public void Start() { }
        public void Deal() { }
        public void Break() { }
    }
}